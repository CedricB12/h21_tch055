# Exercice SQL 7

Vous avez le schema suivant:

![](images/7_schema_figure.png)


* Afin de faire les exercices, vous devez executer le script correspondant a l'exercice que vous pouvez trouvez [ici](scripts/)
* A noter, utiliser le nom des tables/colonnes donner dans le schema ci-haut ou le script (ils sont en anglais)
* VEUILLEZ VALIDER LE NOM DES COLONNES DANS LE CODE DU SCRIPT (ET NON UNIQUEMENT SUR LE ERD) POUR CE NUMERO. Les noms ont changes un peu dans certains cas ou les noms sont des caracteres reservez en SQL.

# Cours 7

1. Sélectionner la liste des clients
2. Sélectionner la liste des employés ayant une salaire qui dépasse 5001$
3. Sélectionner la liste des employés ayant une salaire qui varient de 1000$ à 10000$
4. Sélectionner le CEO
5. Sélectionner les clients ayant le prénom John
6. Sélectionner les clients ayant le prénom qui ne commence pas par 'J'
7. Sélectionner les employés ayant une remarque
8. Créer une vue appelé NotOPlanet contenant les noms des planètes qui ne contient pas la lettre 'o'



# Cours 8

1. Déterminer la moyennes des salaires des employés
2. Déterminer le poids total des packages 'Undeclared'
3. Déterminer qui a recu le package de 1.5kg
4. Déterminer qui a envoyé le package de 1.5kg
5. Déterminer le poids total des packages envoyés par 'Al Gores Head'
6. Grouper les clients par nombres de packages envoyés
6. Parmi les clients qui ont recu des packages déterminer celui qui le poids total minimal.
7. Déterminer les clients qui n'ont pas reçu des packages
8. Déterminer les clients qui ont envoyé et reçu des packages


# Cours 9

1. Afficher les nom des employes et des clients
2. Afficher les nom des employes et des clients ayant un nom de famille qui contient 'Zoi'
3. Augmenter les salaires des employés par 20% si leurs salaires est inférieure à 10000$
4. Engager un livreur ('Delivery boy') appelé "Usain Bolt" avec une salaire de 9000 et une remarque 'Usain est rapide !'
5. Déterminer la liste des employés ayant une salaire qui dàpasse la moyenne des salaire
6. Inscriver la date '3006/05/11' pour les 'shipments' n'ayant pas une date
7. Déterminer le poids total des packages envoyés par le client qui a reçu le package de 1.5kg
8. Le 'Janitor' a démissionné, retirez le de la base de données

## Cours 12
1. Etablir la connection entre la JDBC et votre DB. De plus, pour tous les fonctions, vous devez mettre autocommit = False et completer les transactions a la main.
2. Ecrire une fonction qui insert avec une requete statique un nouveau Products
3. Ecrire une fonction qui insert avec une requete precompilees (tous les colonnes devraient etre parametrable) un nouveau Products
4. Ecrire une fonction qui utilise le code du cours 10 de la procedure 4. Si vous n'avez pas le code de la procedure, vous pouvez aller la chercher sur Gitlab.
5. Ecrire une fonction qui mimique la fonctionalite de la procedure 3 en Java. La fonction ne pourra qu'utiliser les fonctions de LMD (ie: tout refaire le code en Java)
