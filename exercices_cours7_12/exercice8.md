# Exercice SQL 8

Vous avez le schema suivant:

![](images/8_schema_figure.png)


* Afin de faire les exercices, vous devez executer le script correspondant a l'exercice que vous pouvez trouvez [ici](scripts/)
* A noter, utiliser le nom des tables/colonnes donner dans le schema ci-haut ou le script (ils sont en anglais)
* VEUILLEZ VALIDER LE NOM DES COLONNES DANS LE CODE DU SCRIPT (ET NON UNIQUEMENT SUR LE ERD) POUR CE NUMERO. Les noms ont changes un peu dans certains cas ou les noms sont des caracteres reservez en SQL.

# Cours 7
1. Sélectionner la liste des médecins
2. Sélectionner la liste des médecins ayant le prénom John
3. Sélectionner les procédure ayant un prix inférieure à 2000$
4. Sélectionner l'infirmière qualifiée comme 'Head Nurse' 
5. 

# Cours 8

1. Trouver le nom des medecins qui ont performer des procedures medicales pour lesquelles ils n'etaient pas certifier.
2. Obtenir l'information pour les rendez-vous ou un patient a rencontre un medecin different de sont "primary care physician". Lister le nom du patient, le nom du medecin, le nom de l'infirmier (si existe), le debut et la fin du rendez vous, la piece dans lequel le rendez-vous a eu lieu et le nom du "primary care physician" du patient.
3. Obtenir le nom de tous les infirmiers qui on deja ete en appel a la piece 123.
4. Obtenir le nombre de rendez-vous qui a eu lieu dans chaque piece.

# Cours 9
2. Trouver le nom des medecins qui ont performer des procedures medicales pour lesquelles ils n'etaient pas certifier. De plus, ajouter le nom de la procedure, la date (table Undergoes), et le nom du patient.
3. Trouver le nom des medecins qui ont performer des procedures medicales pour lesquelles ils etaient certifies MAIS que la date de la procedure (dans table Undergoes) etaient apres la date d'expiration de la certification.

## Cours 12
1. Etablir la connection entre la JDBC et votre DB. De plus, pour tous les fonctions, vous devez mettre autocommit = False et completer les transactions a la main.
2. Ecrire une fonction qui insert avec une requete statique un nouveau Products
3. Ecrire une fonction qui insert avec une requete precompilees (tous les colonnes devraient etre parametrable) un nouveau Products
4. Ecrire une fonction qui utilise le code du cours 10 de la procedure 4. Si vous n'avez pas le code de la procedure, vous pouvez aller la chercher sur Gitlab.
5. Ecrire une fonction qui mimique la fonctionalite de la procedure 3 en Java. La fonction ne pourra qu'utiliser les fonctions de LMD (ie: tout refaire le code en Java)
